package main;

import jadex.base.Starter;
import jadex.bridge.IComponentIdentifier;
import jadex.bridge.IExternalAccess;
import jadex.bridge.service.RequiredServiceInfo;
import jadex.bridge.service.search.SServiceProvider;
import jadex.bridge.service.types.cms.IComponentManagementService;
import jadex.commons.future.IFuture;
import jadex.commons.future.ThreadSuspendable;
import jadex.micro.annotation.Agent;

/**
 * A simple agent to be used as a basis for own developments.
 */
@Agent
public class Main {

    public static void main(String[] args) {
        String[] defargs = new String[]{
            "-gui", "false",
            "-welcome", "false",
            "-cli", "false",
            "-printpass", "false"
        };
        String[] newargs = new String[defargs.length + args.length];
        System.arraycopy(defargs, 0, newargs, 0, defargs.length);
        System.arraycopy(args, 0, newargs, defargs.length, args.length);
        IFuture<IExternalAccess> platfut = Starter.createPlatform(newargs);
        ThreadSuspendable sus = new ThreadSuspendable();
        IExternalAccess platform = platfut.get(sus);
        System.out.println("Started platform: " + platform.getComponentIdentifier());
        IComponentManagementService cms = SServiceProvider.getService(platform.getServiceProvider(),
        IComponentManagementService.class, RequiredServiceInfo.SCOPE_PLATFORM).get(sus);
        IComponentIdentifier cid = cms.createComponent(null, "jadex.bdiv3.examples.marsworld/MarsWorld3d.application.xml", null, null).get(sus);
        System.out.println("Started chat component: "+cid);
    }
}
